// Pios - Copyright (c) The Pios Authors

#include "driver.h"
#include "driver_def.h"
#include "thirdparty/libfdt/libfdt.h"

#include "driver.list"

#define REGISTER_DRIVER(name) extern const device_driver_description_t name;
DRIVER_LIST
#undef REGISTER_DRIVER

static const device_driver_description_t** get_driver_list();
static const device_driver_description_t* find_driver(
    void* device_tree, const char* node_name, int* pv_device_offset);

static struct {
    const device_driver_description_t* driver;
    uintptr_t handle;
} s_active_serial;

// Initialize serial device. Serial device is the first device which is initialized in pios even
// before mmu. So we can't invoke any malloc/free functions. We will parse the device_tree and
// find out the vendor of this serial device and invoke vendor's driver to continue initialization.
// Ref:
// https://github.com/devicetree-org/devicetree-specification/releases/download/v0.2/devicetree-specification-v0.2.pdf
int serial_init(void* device_tree)
{
    // Get the serial0 driver.
    int device_offset = 0;
    const device_driver_description_t* serial_driver
        = find_driver(device_tree, "serial0", &device_offset);
    if (!serial_driver) {
        // No serial0? Let's try "serial".
        serial_driver = find_driver(device_tree, "serial", &device_offset);
        if (!serial_driver) {
            // Can't find the driver, return error.
            return -1;
        }
    }

    // Found the driver, initialize it.
    uintptr_t handle = 0;
    init_command_parameter_t parameter
        = { .device_tree = device_tree, .device_offset = device_offset, .pv_handle = &handle };

    // If initalize success, save it.
    int res = serial_driver->func("init", &parameter);
    if (res) {
        s_active_serial.driver = serial_driver;
        s_active_serial.handle = handle;
    }
    return res;
}

int serial_putc(char ch)
{
    if (!s_active_serial.driver) {
        // Not initialized, return error.
        return -1;
    }

    write_command_parameter_t parameter
        = { .handle = s_active_serial.handle, .size = 1, .pv_buffer = &ch };
    return s_active_serial.driver->func("write", &parameter);
}

int serial_puts(const char* p)
{
    if (!s_active_serial.driver) {
        // Not initialized, return error.
        return -1;
    }

    if (!p) {
        return 0;
    }

    write_command_parameter_t parameter
        = { .handle = s_active_serial.handle, .size = strlen(p), .pv_buffer = p };
    return s_active_serial.driver->func("write", &parameter);
}

static const device_driver_description_t** get_driver_list()
{
    static const device_driver_description_t* drivers[] = {
#define REGISTER_DRIVER(name) &name,
        DRIVER_LIST
#undef REGISTER_DRIVER
            NULL
    };
    return drivers;
};

static const device_driver_description_t* find_driver(
    void* device_tree, const char* node_name, int* pv_device_offset)
{
    // Get the serial0.
    *pv_device_offset = fdt_path_offset(device_tree, node_name);
    if (*pv_device_offset < 0) {
        // No such node?
        return NULL;
    }

    // Let's find out the vendor's name.
    int compatibles_total_bytes = 0;
    const char* compatible = (const char*)fdt_getprop(
        device_tree, *pv_device_offset, "compatible", &compatibles_total_bytes);
    if (!compatible || compatibles_total_bytes < 0) {
        // No compatible? It's required.
        return NULL;
    }

    // Find the driver who can process this device.
    int compatible_checked_bytes = 0;
    for (;;) {
        // Go through the driver list.
        for (const device_driver_description_t** support_drivers = get_driver_list();
             *support_drivers; ++support_drivers) {
            // Go through the driver's compatible list, find the match one.
            for (const char* const* driver_compatibles = (*support_drivers)->compatibles;
                 *driver_compatibles; ++driver_compatibles) {
                if (!strcmp(compatible, *driver_compatibles)) {
                    // Found the driver.
                    return *support_drivers;
                }
            }
        }

        // Haven't found. It doesn't mater. Device may have multiple compatibles. Let's try the next
        // one. Jump to the next compatible.
        int len = strlen(compatible) + 1;
        compatible_checked_bytes += len;
        compatible += len;

        // No more compatible?
        if (compatible_checked_bytes >= compatibles_total_bytes) {
            break;
        }
    }

    // Not found.
    return NULL;
}