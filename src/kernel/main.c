// Pios - Copyright (c) The Pios Authors

#include "src/driver/driver.h"
#include "thirdparty/libfdt/libfdt.h"

// Follow booting document: https://www.kernel.org/doc/Documentation/arm64/booting.txt.
// The fist parameter (passed via x0) is the physical address of device tree blob.
int main(void* device_tree)
{
    // Cehck whether the device tree is ready for use. We only support booting with device tree.
    if (!device_tree || fdt_check_header(device_tree)) {
        // Fatal error!!!
        return -1;
    }

    // Let's initialize serialize port first, so that we can output debug information ASAP.
    serial_init(device_tree);
    serial_puts("Pios is booting...\r\n");
    return 0;
}
