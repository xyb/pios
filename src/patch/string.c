// Pios - Copyright (c) The Pios Authors

#include <stdint.h>
#include <string.h>

// The strlen in newlibc which is used by gcc-aarch64-elf doesn't support unaligned memory access.
// This version support it.
size_t strlen(const char* str)
{
    uintptr_t s = (uintptr_t)str;
    while (*str++)
        ;
    return (uintptr_t)str - s - 1;
}
