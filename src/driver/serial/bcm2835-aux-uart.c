// Pios - Copyright (c) The Pios Authors

#include "../driver_def.h"
#include "thirdparty/libfdt/libfdt.h"
#include <stddef.h>
#include <stdint.h>

static int init(init_command_parameter_t* parameter);
static int write(write_command_parameter_t* parameter);
static uintptr_t get_io_base(void* device_tree, int device_offset);
static inline uint32_t io_read(uintptr_t address) { return *(volatile uint32_t*)address; }
static inline void io_write(uintptr_t address, uint32_t data)
{
    *(volatile uint32_t*)address = data;
}

// Initialize mini uart.
// Ref:
// https://github.com/raspberrypi/documentation/blob/master/hardware/raspberrypi/bcm2835/BCM2835-ARM-Peripherals.pdf
static int process_command(const char* command, void* parameter)
{
    if (!strcmp(command, "init")) {
        return init((init_command_parameter_t*)parameter);
    } else if (!strcmp(command, "write")) {
        return write((write_command_parameter_t*)parameter);
    }
}

static int init(init_command_parameter_t* parameter)
{
    // Get aux device.
    int aux_offset = fdt_path_offset(parameter->device_tree, "aux");
    if (!aux_offset) {
        // aux controller is required.
        return -1;
    }

    // Get the aux address.
    uintptr_t aux_base = get_io_base(parameter->device_tree, aux_offset);
    if (!aux_base) {
        // Return if we can't get the device address in memory.
        return -1;
    }

    // Enable UART in aux register. (ref: 2 Auxiliaries: UART1 & SPI1, SPI2)
    io_write(aux_base + 0x4, io_read(aux_base + 0x4) | 1);
    *parameter->pv_handle = aux_base;
}

static int write(write_command_parameter_t* parameter)
{
    uintptr_t aux_base = (uintptr_t)parameter->handle;

    for (size_t i = 0; i < parameter->size; ++i) {
        // Wait device until it can accept one byte.
        // See ref AUX_MU_LSR_REG Register.
        while (!(io_read(aux_base + 0x54) & 0x30))
            ;

        // Write one byte.
        io_write(aux_base + 0x40, parameter->pv_buffer[i]);
    }
}

// Return the io base address of the device.
// Ref:
// https://github.com/devicetree-org/devicetree-specification/releases/download/v0.2/devicetree-specification-v0.2.pdf
static uintptr_t get_io_base(void* device_tree, int device_offset)
{
    // Let's get the #address_cells from parent node first. We need it to decode the real
    // address.
    int parent_offset = fdt_parent_offset(device_tree, device_offset);
    if (parent_offset < 0) {
        // No parent offset? Impossible. Follow the ref, all nodes except root should have a
        // parent.
        return 0;
    }

    int address_cells = fdt_address_cells(device_tree, parent_offset);
    int size_cells = fdt_size_cells(device_tree, parent_offset);
    if (address_cells <= 0 || address_cells > 2 || size_cells <= 0 || size_cells > 2) {
        // No #address_cells or #size_cells? Impossible. Follow the ref, all nodes which has
        // children should have #address_cells and #size_cells. At meanwhile, we only process 32
        // bits or 64 bits address/size. So the cells number should be 1 or 2.
        return 0;
    }

    // Now, let's get the address of this device.
    int reg_length = 0;
    uintptr_t pv_reg_property
        = (uintptr_t)fdt_getprop(device_tree, device_offset, "reg", &reg_length);
    if (!pv_reg_property || !reg_length) {
        // No reg property? Error, this property is required.
        return 0;
    }

    // Follow the ref, the format of reg should be <address, size>.
    // The size of address and size are determined by address_cells and size_cells.
    // Each cell is an unit32_t value (big-ending).
    uintptr_t io_base_in_bus = 0;
    if (address_cells == 1) {
        // 32 bits address.
        io_base_in_bus = fdt32_to_cpu(*(uint32_t*)pv_reg_property);
    } else {
        // address_cells must be 2 here, 64 address.
        io_base_in_bus = fdt64_to_cpu(*(uint64_t*)pv_reg_property);
    }

    // Follow the ref, the address got from reg is the bus address, we need map it to physic
    // address. The parent ranges property will be used to do such work. The format of ranges
    // property is <child-bus-address, parent-buss-address, length>
    while (parent_offset >= 0) {
        int ranges_length = 0;
        uintptr_t pv_ranges_property
            = (uintptr_t)fdt_getprop(device_tree, parent_offset, "ranges", &ranges_length);
        if (pv_ranges_property) {
            int checked_lenth = 0;
            while (checked_lenth < ranges_length) {
                // Get parent address.
                uintptr_t parent_address_base = 0;
                uintptr_t child_address_base = 0;

                if (address_cells == 1) {
                    parent_address_base = fdt32_to_cpu(*(uint32_t*)pv_ranges_property);
                    child_address_base = fdt32_to_cpu(*((uint32_t*)pv_ranges_property + 1));
                    // move to size field.
                    pv_ranges_property += sizeof(uint32_t) * 2;
                    checked_lenth += sizeof(uint32_t) * 2;
                } else {
                    parent_address_base = fdt64_to_cpu(*(uint64_t*)pv_ranges_property);
                    child_address_base = fdt64_to_cpu(*((uint64_t*)pv_ranges_property + 1));
                    // move to size field.
                    pv_ranges_property += sizeof(uint64_t) * 2;
                    checked_lenth += sizeof(uint64_t) * 2;
                }

                // Get address length.
                uintptr_t address_length = 0;
                if (size_cells == 1) {
                    address_length = fdt32_to_cpu(*(uint32_t*)pv_ranges_property);
                    // move to next range.
                    pv_ranges_property += sizeof(uint32_t);
                    checked_lenth += sizeof(uint32_t);
                } else {
                    address_length = fdt32_to_cpu(*(uint64_t*)pv_ranges_property);
                    // move to next range
                    pv_ranges_property += sizeof(uint64_t);
                    checked_lenth += sizeof(uint64_t);
                }

                // Is the child address in this range?
                if (io_base_in_bus >= parent_address_base
                    && io_base_in_bus < parent_address_base + address_length) {
                    // Found the range, let's map it.
                    io_base_in_bus = io_base_in_bus - parent_address_base + child_address_base;
                    break;
                }
            }
        }
        // Check parent os parent.
        parent_offset = fdt_parent_offset(device_tree, parent_offset);
    }
    return io_base_in_bus;
}

// clang-format off
const device_driver_description_t bcm_2835_uart_driver =
{
    .name = "UART driver for Broadcom 2835",
    .description = "This is serial port driver for Boradcom 2835.",
    .author = "xyb <xyb@xyb.name>",
    .version = "1.0",
    .license = "GPLv2",
    .func = process_command,
    .compatibles = { 
        "brcm,bcm2835-aux-uart",
        NULL
    }
};
// clang-format on
