// Pios - Copyright (c) The Pios Authors

#pragma once

int serial_init(void* device_tree);
int serial_putc(char ch);
int serial_puts(const char* p);