// Pios - Copyright (c) The Pios Authors

.global _start
.section .text.boot
_start:
    ldr x19, =_start
    mov sp, x19
    b main

1:
    wfe
    b 1b
