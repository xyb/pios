*Pios - Copyright (c) The Pios Authors*

# Pios

## Build
### On Linux 18.10.
1. Download GCC Toolchain.

        wget https://developer.arm.com/-/media/Files/downloads/gnu-a/8.2-2018.08/gcc-arm-8.2-2018.08-x86_64-aarch64-elf.tar.xz
        tar xf gcc-arm-8.2-2018.08-x86_64-aarch64-elf.tar.xz

1. Install Dependencies.

        sudo apt-get install xz-utils cmake ninja-build

1. Create a build folder and generate ninja build files.

        mkdir -p ~/pios-build
        cd ~/pios-build
        cmake <path-to-pios-source-code>/ -DTOOLCHAIN=<path-to-gcc-aarch64-elf-toolchain> -GNinja

1. Build

        ninja

### On Windows 10
1. Download GCC Toolchain.

        https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/8.2-2018.08/gcc-arm-8.2-2018.08-i686-mingw32-aarch64-elf.tar.xz

1. Install Dependencies.

        CMake >= 3.12.
        ActivatePerl >= 5.26 (select "run .pl file directly" when you install).
        Ninja >= 1.7.2.
        7z >= 16.04.

    CMake >= 3.12 is required. For others, old version may be OK.

1. Create a build folder and generate ninja build files.

        mkdir -p ~/pios-build
        cd ~/pios-build
        cmake <path-to-pios-source-code>/ -DTOOLCHAIN=<path-to-gcc-aarch64-elf-toolchain> -GNinja

    Use `/` instead of `\` in path. For example, use `d:/src/gcc-arm-toolchain` instead of
    `d:\src\gcc-arm-toolchain` or `d:\\src\\gcc-arm-toolchain`.

1. Build

        ninja