// Pios - Copyright (c) The Pios Authors

#pragma once

#include <stddef.h>
#include <stdint.h>

// Any driver must be implemented a function to do interaction with kernel.
// For all invocations, return 0 means success, other values means error.
//
// Command:
//      "init": Initialize driver. All drivers must implement this command. Parameter is a pointer
//      to init_command_parameter_t.
//
//      "write": Write data to the driver. Parameter is a pointer to write_command_parameter_t.
typedef int (*driver_func_t)(const char* command, void* parameter);

typedef struct {
    /*in*/ void* device_tree;
    /*in*/ int device_offset;
    /*out*/ uintptr_t* pv_handle;
} init_command_parameter_t;

typedef struct {
    /*in*/ uintptr_t handle;
    /*in*/ size_t size;
    /*in*/ const uint8_t* pv_buffer;
} write_command_parameter_t;

typedef struct {
    // Name of the driver.
    const char* name;
    // Description of the driver.
    const char* description;
    // Author name of the driver.
    const char* author;
    // Version of the driver.
    const char* version;
    // License of the driver.
    const char* license;
    // Initialzation function.
    driver_func_t func;
    // Compatible list, must be ended with NULL item.
    const char* compatibles[];
} device_driver_description_t;
